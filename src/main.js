var signOutButton = document.getElementById('signout_button');
signOutButton.addEventListener("click",signOut);
listFiles();

/**
*  Sign out the user upon button click.
*/
function signOut(event) {
    gapi.auth2.getAuthInstance().signOut();
    location.href = "./index.html";
    console.log("Signed Out");
}


/**
       * Print files.
       */
function listFiles() {
    gapi.client.drive.files.list({
    'pageSize': 10,
    'fields': "nextPageToken, files(id, name)"
    }).then(function(response) {
    appendPre('Files:');
    var files = response.result.files;
    if (files && files.length > 0) {
        for (var i = 0; i < files.length; i++) {
        var file = files[i];
        console.log(file.name + ' (' + file.id + ')');
        }
    } else {
        console.log('No files found.');
    }
    });
}


/*let fileContent;

document.getElementById('inputfile')
            .addEventListener('change', function() {
            
            var fr=new FileReader();
            fr.onload=function(){
                fileContent = fr.result;
                console.log(fileContent);
                document.getElementById('output')
                        .textContent=fr.result;
            }
            
            fr.readAsText(this.files[0]);
        })*/

/*const encryptedString = rc4.encrypt(fileContent,'key');
console.log(encryptedString);
const decryptedString = rc4.decrypt(encryptedString, 'key');
console.log(decryptedString);*/