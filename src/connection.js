// Client ID and API key from the Developer Console
const CLIENT_ID = '120302697004-onbjckkl8ol8eos9ohn60opqiebdft8g.apps.googleusercontent.com';
const API_KEY = 'AIzaSyDQbQyGrP9flTg0IvLqvdBnw9QItf_zthY';

 // Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

 // Authorization scopes required by the API; multiple scopes can be
 // included, separated by spaces.
const SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly';

function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}

function initClient() {
    gapi.client.init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
    }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

      // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    
    }, function(error) {
        appendPre(JSON.stringify(error, null, 2));
        alert("Connection à l'API Google impossible");
    });
}

/**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
function updateSigninStatus(isSignedIn) {
    
}