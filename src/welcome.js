var signInButton = document.getElementById('signin_button');
signInButton.addEventListener("click",signIn);

/**
    *  Sign in the user upon button click.
    */
function signIn(event) {
    gapi.auth2.getAuthInstance().signIn();
    location.href = "./app.html";
    console.log("Signed In");
    
}